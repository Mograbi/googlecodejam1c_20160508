/*
 * C.cpp
 *
 *  Created on: May 8, 2016
 *      Author: Moawiya
 */




#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>    // std::sort
#include <vector>       // std::vector
#include <map>
using namespace std;

int main() {
	std::ifstream fin("c.in");

	std::ofstream fout("c.out");
//	std::ifstream fin("A-small.in");
//	FILE *fout = freopen("A-small.out", "w", stdout);
	int T;
	fin >> T;
	for (int t = 1; t <= T; t++) {
		int J,P,S, K, lines;
		lines = 0;
		fin >> J;
		fin >> P;
		fin >> S;
		fin >> K;
		stringstream ss;
		for (int j=1;j<=J;j++) {
			for (int p=1;p<=P;p++) {
				for (int s=1;s<=S;s++) {
					stringstream temp;
					temp << j << " " << p << " " << s << endl;

					lines++;
				}
			}
		}
		fout << "Case #" << t << ": " << lines << endl;
		fout << ss.str() << endl;
	}
	//exit(0);

	return 0;
}

